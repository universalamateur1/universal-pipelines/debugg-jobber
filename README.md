# CI/CD component & Catalog Demo Project

Moved to [GitLab Learn Labs/CS Shared Demo Space/CICD CICD/Demo-CICD-component-catalog](https://gitlab.com/gitlab-learn-labs/webinars/cicd/demo-cicd-component-catalog)


## Goal and Purpose

Easy Out of the box Demo for CS at GitLab to demonstrate the functionality of the GitLab Feature [CI/CD Catalog](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) and [CI/CD components](https://docs.gitlab.com/ee/ci/components/index.html).

## What you will show

1. The inclusion of a GitLab Maintained CI component into a small Pipeline in this Project.
2. The creation of a straightforward CI Component.
3. The Publication of the created CI Component into [the CI/CD Catalog on GitLab.com](https://gitlab.com/explore/catalog/)
4. The integration of just created Component into the same Pipeline.

## Usage

1. Fork this Project into your own Demo GitLab.com namespace.
2. __Optional__ Watch [the Walkthrough Video](url)
3. Follow the [Runbook](./demo-runbook-CI-Component.md).
4. Follow the [break down steps](./demo_breakdown.md).

## ToDo

- [x] Dynamic Job Names
- [x] Add Logo and Banner
- [ ] Optimize CI Component
- [ ] Publish CI Component into a shared space
- [ ] Ingest Ci Component into Demo Project via Git Submodules
